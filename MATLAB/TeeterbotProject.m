clear all; close all; clc;

m1 = 2;
m2 = 10;
l = 0.8;
J = (1/3)*m2*l^2;
g=9.81;

alpha = (m2^2*l^2)/((m1+m2)*(m2*l^2+4*J));
beta = 2*m2*l/((m1+m2)*(m2*l^2+4*J));
gamma = m2*l/(2*(m1+m2));
omega = 2*m2*l*g/(m2*l^2+4*J);
phi = 1/(m1+m2);

A = [0 -(alpha*g/(1-alpha)) 0; 0 0 1; 0 omega/(1-alpha) 0];
B = [phi/(1-alpha); 0; -(beta/(1-alpha))];
C = [1 1 1];
D = [0];

p = [-1,-5,-10];
T = 0.05;

K1 = place(A, B, p);

rankCab = rank(ctrb(A,B));
eigA = eig(A);

sys = ss(A,B,C,0);

Q = C'*C;
Q(1,1) = 5000;
Q(3,3) = 100;
R = 1;
K = lqr(A,B,Q,R);

Ac = [(A-B*K)];
Bc = [B];
Cc = [C];
Dc = [D];

eigAc = eig(Ac);

T = 0.05;

sys_cl = ss(Ac,Bc,Cc,Dc);
sys_d = c2d(sys,T);

I = eye(3);
N = dcgain(sys_cl);
