// Include guard to prevent multiple declarations
#ifndef CONTROLLER_H
#define CONTROLLER_H

// ROS header
#include <ros/ros.h>
#include <eigen3/Eigen/Dense>
//#include <visualization_msgs/MarkerArray.h>
#include <tf/tf.h>
//#include <tf/transform_broadcaster.h>
//#include <ugv_course_libs/gps_conv.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/Imu.h>
//#include <nav_msgs/Path.h>
#include "gazebo_msgs/LinkStates.h"

// Namespace matches ROS package name
namespace controller{

class controller
{
public:
  controller(ros::NodeHandle n, ros::NodeHandle pn);
  
private:
    void timerCallback(const ros::TimerEvent& event);
    double calculateForce(Eigen::Vector3d currentXstate);
    void recvData(const sensor_msgs::Imu::ConstPtr &msg);
    void recvData2(const gazebo_msgs::LinkStatesPtr &msg);
    void recvSpeed(const std_msgs::Float64 &msg);
    
    ros::Timer timer;
    ros::Subscriber sub_data;
    ros::Subscriber sub_data2;
    ros::Subscriber sub_speed;
    ros::Publisher pub_left_torque;
    ros::Publisher pub_right_torque;
    std_msgs::Float64 torque_msg;
    
    double sample_time;
    double u;
    double delta_time;
    double refInput;
     
    Eigen::Matrix<double, 1, 3> K;
    Eigen::Matrix3d A;
    Eigen::Vector3d B;
    Eigen::Matrix3d C;
    Eigen::Vector3d stateX;
    Eigen::Vector3d stateX2;
    Eigen::Vector3d stateXdot;
    Eigen::Vector3d newstateX;
};

}

#endif // CONTROLLER_H

