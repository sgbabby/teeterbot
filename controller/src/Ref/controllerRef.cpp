#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <ugv_course_libs/gps_conv.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/NavSatFix.h>
#include <nav_msgs/Path.h>

// Macro to change degrees to radions
#define degreesToRadians(angleDegrees) (angleDegrees * M_PI / 180.0)

// Defining variables to be used in the program
ros::Publisher waypointsMarkers_pub;
visualization_msgs::MarkerArray WaypointMarkers;
tf::StampedTransform transform;
ros::Publisher cmd_vel;
geometry_msgs::Twist vel_msg;

// Function used for calculations such as distance to waypoint, angle correction, and speed
void calculateDistance() {

}

// Timer callback that initiates the calculations, tranform, path traveled, publish waypoint markers, and publish audibot velocity and yaw rate
void timerCallback(const ros::TimerEvent& event){
    calculateDistance();
    
    static tf::TransformBroadcaster broadcaster;
    transform.stamp_ = event.current_real;
    transform.frame_id_ = "world";
    transform.child_frame_id_ = "marker";
    transform.setOrigin(tf::Vector3(0,0,0));
    transform.setRotation(tf::Quaternion(0,0,0,1));
    broadcaster.sendTransform(transform);
    
    cmd_vel.publish(vel_msg);
}

// Recieve current audibot position from GPSfix
void recvFix(const sensor_msgs::NavSatFixConstPtr& msg){
    UTMCoords current_coords (*msg);    
}

int main(int argc, char** argv){
    ros::init(argc, argv, "controller");
    ros::NodeHandle nh; 
    
    // Subscribe to audibot gps fix and heading messages
    ros::Subscriber sub_fix = nh.subscribe("audibot/gps/fix", 1, recvFix);
    
    // Create timer that runs 20 times a second
    ros::Timer timer = nh.createTimer(ros::Duration(0.05),timerCallback); 
    
    // Advertise audibot velocity and traveresed gps path 
    cmd_vel = nh.advertise<geometry_msgs::Twist>("/audibot/cmd_vel",1);
    
    ros::spin();
}
