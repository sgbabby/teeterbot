// Header file for the class
#include "controller.h"

// Namespace matches ROS package name
namespace controller {

// Constructor with global and private node handle arguments
controller::controller(ros::NodeHandle n, ros::NodeHandle pn)
{
    sample_time = 0.05;
    delta_time = 0.05;
    
    // Initialize state space coniditions
    stateX << 0, 0, 0;
    stateX2 << 0, 0, 0;
    stateXdot << 0, 0, 0;
    newstateX << 0, 0, 0;
    u = 0; 
    refInput = 0;
    
    // Initial calculated K matrix 
    K << -70.7107, -634.5758, -182.5796;
    
    // Second calculated K matrix to test
    //K << -36.6972, -585.7200, -149.4508;
    
    // Testing with continuous A,B,C matrices
    A << 0,   -5.4500,       0,
             0,         0,    1.0000,
             0,   16.3500,         0;
          
    B <<   0.1296,
            0,
            -0.1389;
    C << 1,   0,    0,
        0,   1,     0,
        0,   0,     1;
    
    // Testing with discrete A,B,C matrices. Calculeted using matlab command with zoh for 0.05 period.
    /*
    A << 1,   -0.2744,       -0.006836,
             0,         1.021,    0.05034,
             0,   0.8231,         1.021;          
    B <<   0.006497,
            -0.0001742,
            -0.006992;
    C << 1,   0,    0,
        0,   1,     0,
        0,   0,     1;  
    */        
    timer = n.createTimer(ros::Duration(sample_time), &controller::timerCallback, this);
    sub_data = n.subscribe("/teeterbot/imu/",1,&controller::recvData,this);
    sub_data2 = n.subscribe("/gazebo/link_states/",1,&controller::recvData2,this);
    sub_speed = n.subscribe("/input/speed/",1,&controller::recvSpeed,this);
    pub_left_torque = n.advertise<std_msgs::Float64>("/teeterbot/left_torque_cmd", 1);
    pub_right_torque = n.advertise<std_msgs::Float64>("/teeterbot/right_torque_cmd", 1);
}

double controller::calculateForce(Eigen::Vector3d currentXstate)
{
    double outputForce, outputFeedback;
    // input current state (x) vector data from imu sensor
    Eigen::Vector3d inputXstate;
    inputXstate = currentXstate;
    // calculate state dot (xdot) from current imu input data and control design from teeterbot pdf 
    stateXdot = (A * inputXstate) + (B * u);
    // new state (x) data to output is calculated by integrating state dot (xdot)
    newstateX = inputXstate + (stateXdot * delta_time);
    // feedback input from K matrix and outputed newstateX
    outputFeedback = K * newstateX;
    // u is equal to the reference minus the feedback to track an inputted speed
    u =  refInput - outputFeedback;
    // feedback output is also F=-kx for torque command. Divided by 2 because 2 wheels.
    outputForce = -1*outputFeedback/2;
    return outputForce;
}

void controller::timerCallback(const ros::TimerEvent& event)
{
    // pass current state data inputs from imu sensor. Recieve back F/2 where F = -k*x from pdf model. F/2 because force is split between two wheels. 
    torque_msg.data = controller::calculateForce(stateX);
    pub_left_torque.publish(torque_msg);
    pub_right_torque.publish(torque_msg);
}

void controller::recvData(const sensor_msgs::Imu::ConstPtr &msg)
{
    double xdot, xdotdot, theta, thetadot;
    xdot = xdotdot = theta = thetadot = 0;
    /* 
     * Get current state data from imu sensor
     * imu sensor gives linear acceleration and angular velocity data from teeterbot.
     * state matrix described in pdf as 3x1 matrix = [xdot;theta;thetadot]
     * thetadot directly from angular velocity.y imu sensor
     * xdot is calculated by integrating xdotdot from imu sensor
     * theta is calculed by integrating thetadot from imu sensor
     */
    xdotdot = msg->linear_acceleration.x;
    thetadot = msg->angular_velocity.y;
    xdot += xdotdot * delta_time;
    theta += thetadot * delta_time;
    stateX << xdot, theta, thetadot;
}

void controller::recvData2(const gazebo_msgs::LinkStatesPtr &msg)
{
    double x, xdot, theta, thetadot;
    double roll, pitch, yaw;
    double preX, preTheta = 0; 
    x = xdot = theta = thetadot = 0;
    /*
     * Second method of inputting data from teeterbot.
     * Here I look at only the orange body of the teeterbot.
     * state matrix described in pdf as 3x1 matrix = [xdot;theta;thetadot]
     * Convert quaternion to rpy from pose message.
     * theta is the pitch of the rpy. 
     * thetadot is calculated by taking the derivative of theta
     * xdot is calculated by taking the derivative of the x position of the teeterbot body
    */
    tf::Quaternion quat;
    tf::quaternionMsgToTF(msg->pose[1].orientation, quat);
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);
    geometry_msgs::Vector3 rpy;
    rpy.y = pitch;
    x = msg->pose[1].position.x;
    theta = pitch;
    xdot = (x - preX)/delta_time;
    thetadot = (theta - preTheta)/delta_time;
    preX = x; 
    preTheta = theta;
    stateX2 << xdot, theta, thetadot;
}

void controller::recvSpeed(const std_msgs::Float64 &msg)
{
    double inputSpeed;
    Eigen::Vector3d desiredOutput;
    inputSpeed = msg.data;
    desiredOutput << inputSpeed, 0,0;
    refInput = K*desiredOutput;
}

}
