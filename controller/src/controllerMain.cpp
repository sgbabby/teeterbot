// ROS and node class header file
#include <ros/ros.h>
#include "controller.h"

int main(int argc, char** argv)
{
  // Initialize ROS and declare node handles
  ros::init(argc, argv, "controller");
  ros::NodeHandle n;
  ros::NodeHandle pn("~");
  
  // Instantiate node class
  controller::controller node(n, pn);
  
  // Spin and process callbacks
  ros::spin();
}
